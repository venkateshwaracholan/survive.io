

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var debug = require('debug')('sailio:server');
var mainRoute = require('./routes/mainRoute');
var UpdateUtils = require('./public/js/UpdateUtils.js');
var GameEntity = require('./public/js/GameEntity.js');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var port = 7000;
var server = http.listen(port);
server.on('error', onError);
server.on('listening', onListening);

//GAME STRUCTURES
var map_width = 500;
var map_height = 500;
var objectscale = .1;
var view_scale = 3000 * objectscale;

var entityVsInventoryMap = {"STONE": "stone", "TREE": "wood"}

function Rewards() {
    this.stone = 0;
    this.wood = 0;
}

var stationaryEntities = {};
var mobileEntities = {};
var stonesize = 10;
addstones();
addAnimals();
addTrees();
var stoneCount = 20;
var treeCount = 4;

function addstones() {
    var stoneSizes = [10, 12, 15, 9, 11];
    var stoneX = [418, 85, 459, 260, 331, 388, 431, 371, 450, 267, 38, 427, 255, 109, 305, 256, 7, 163, 92, 185];
    var stoneY = [274, 147, 187, 400, 296, 413, 2, 371, 60, 111, 263, 410, 455, 176, 350, 88, 187, 181, 334, 434];
    for (var i = 0; i < stoneX.length; i++) {
        var type = 'STONE';
        stationaryEntities[type + '_' + i] = new GameEntity.StationaryEntity(type + '_' + i, stoneX[i], stoneY[i], stoneSizes[i % 5], type);
    }
}

function addAnimals() {

    for (var i = 1; i < 5; i++) {
        var type = 'ANIMAL';
        mobileEntities[type + '_' + i] = new GameEntity.MobileEntity(type + '_' + i, i * 50, i * 50, 10, type, .5);
    }
}

function addTrees() {
    var treeSizes = [10, 12, 15, 9, 11];
    var treeX = [100, 300, 400, 500];
    var treeY = [100, 300, 200, 100];
    for (var i = 0; i < treeX.length; i++) {
        var type = 'TREE';
        stationaryEntities[type + '_' + i] = new GameEntity.StationaryEntity(type + '_' + i, treeX[i], treeY[i], treeSizes[i % 5], type);
    }
}


var allStones = [];

setInterval(function () {
    increaseHealthPeriodically("STONE", stoneCount);
    increaseHealthPeriodically("TREE", treeCount);
}, 3000);

function increaseHealthPeriodically(type, count)
{
    for (var i = 0; i < count; i++)
    {
        stationaryEntities[type + '_' + i].stats.health++;
        if (stationaryEntities[type + '_' + i].stats.health > stationaryEntities[type + '_' + i].stats.size * 4)
        {
            stationaryEntities[type + '_' + i].stats.health = stationaryEntities[type + '_' + i].stats.size * 4;
        }
    }
}

setInterval(serverLoop, 16.66);
function serverLoop()
{
    for (var key in mobileEntities)
    {
        var animal = mobileEntities[key];
        animal.stats.status = '';
        var targetPlayer = playerMap[animal.stats.last_hit_by];
        var animalRadius = animal.stats.size * 4;
        var targetPlayerInRange;
        if (targetPlayer)
        {
            targetPlayerInRange = UpdateUtils.circularCollision(targetPlayer.movement.targetx, targetPlayer.movement.targety, targetPlayer.stats.size, animal.movement.mapx, animal.movement.mapy, animalRadius)
        }

        if (!targetPlayer || !targetPlayerInRange)
        {
            for (var id in playerMap)
            {
                var player = playerMap[id];

                if (UpdateUtils.circularCollision(player.movement.targetx, player.movement.targety, player.stats.size, animal.movement.mapx, animal.movement.mapy, animalRadius))
                {
                    animal.stats.last_hit_by = id;
                    targetPlayer = player;
                    break;
                }
            }
        }

        if (targetPlayer && UpdateUtils.circularCollision(targetPlayer.movement.targetx, targetPlayer.movement.targety, targetPlayer.stats.size, animal.movement.mapx, animal.movement.mapy, animalRadius))
        {
            var targetx = targetPlayer.movement.targetx, targety = targetPlayer.movement.targety;
            var entityx = animal.movement.mapx, entityy = animal.movement.mapy;
            var actdist = Math.sqrt((targetx - entityx) * (targetx - entityx) + (targety - entityy) * (targety - entityy));
            if (actdist > targetPlayer.stats.size * 2)
            {
                UpdateUtils.followRepelTarget(animal, targetPlayer.movement.targetx, targetPlayer.movement.targety, 'follow', 'rigid');
            }
            animal.movement.angle = UpdateUtils.calculateAngle(animal.movement.mapx, animal.movement.mapy, targetPlayer.movement.targetx, targetPlayer.movement.targety);
            if (animal.lefthand.hit == 0)
            {
                animal.lefthand.hit = 1;
            }
            UpdateUtils.handAttack(animal.lefthand);
            if (animal.lefthand.attack === 1)
            {
                var attackxy = UpdateUtils.getPointInAngle(animal.movement.mapx, animal.movement.mapy, animal.movement.angle, 7);
                if (UpdateUtils.circularCollision(attackxy[0], attackxy[1], difficultyCircleRadius, targetPlayer.movement.targetx, targetPlayer.movement.targety, targetPlayer.stats.size))
                {
                    UpdateUtils.reduceStat(targetPlayer, 'health', 0, 10);
                    targetPlayer.stats.status = 'hurt';
                }
            }
        } else
        {
            animal.stats.last_hit_by = '';
        }
        var otherAnimals = JSON.parse(JSON.stringify(mobileEntities));
        delete otherAnimals[key];
        UpdateUtils.preventMovementAfterCircularCollision(animal, stationaryEntities);
        UpdateUtils.preventMovementAfterCircularCollision(animal, otherAnimals);
    }

}

var playerMap = {};
var playerSocketMap = {};
var uid = 1;
gameprops = {
    "map_width": map_width,
    "map_height": map_height,
    "objectscale": objectscale
}
var difficultyCircleRadius = 4;

io.on('connection', function (socket) {

    console.log('a user connected');

    socket.on('joinGame', function (name) {
        var pid = uid;
        uid++;
        var objs = gameprops["objectscale"];
        console.log(this.id);
        console.log(pid);
        var newPlayer = new GameEntity.PlayerEntity(pid, 100, 50, 6, 'white', .6);
        newPlayer.name = name;
        newPlayer.score = 0;
        playerMap[pid] = newPlayer;
        playerSocketMap[socket.id] = pid;
        socket.emit('joinSuccess', JSON.stringify([newPlayer, gameprops]));
    });

    socket.on('disconnect', function () {
        var pid = playerSocketMap[socket.id];
        delete playerMap[pid];
        delete playerSocketMap[socket.id];
        console.log('user disconnected');
        socket.broadcast.emit('playerRemoved', pid);
    });

    socket.on('clientUpdateToServer', function (serialData) {

        var clientData = JSON.parse(serialData);
        var hero = clientData["hero"];
        var resourceSpent = clientData["resourceSpent"];
        var pid = hero["uniqueId"]
        var playerDataInServer = playerMap[pid];
        playerDataInServer['movement']['angle'] = hero['movement']['angle'];
        playerDataInServer['movement']['targetx'] = hero['movement']['targetx']
        playerDataInServer['movement']['targety'] = hero['movement']['targety']
        playerDataInServer['movement']['mapx'] = hero['movement']['mapx']
        playerDataInServer['movement']['mapy'] = hero['movement']['mapy']
        playerDataInServer['lefthand'] = hero['lefthand']
        playerDataInServer['righthand'] = hero['righthand']

        //hereafter use the playerDataInServer // dont use hero....it will not be sent back to client. 
        var clients = io.sockets.sockets;
        removeUnwantedPlayers(Object.keys(clients));

        var outData = {};
        var rewards = new Rewards();
        rewards.resourceEmpty = false;


        checkCollisionAndUpdateConsequences(playerDataInServer, playerDataInServer.lefthand, rewards);
        checkCollisionAndUpdateConsequences(playerDataInServer, playerDataInServer.righthand, rewards);

        for (var key in resourceSpent) {
            playerDataInServer["inventory"][key] -= resourceSpent[key];
        }


        var tempPlayerMap = JSON.parse(JSON.stringify(playerMap));
        delete tempPlayerMap[pid];
        outData['heroFromServer'] = playerDataInServer;
        outData['otherPlayersData'] = tempPlayerMap;
        outData['stationaryEntities'] = stationaryEntities;
        outData['mobileEntities'] = mobileEntities;
        outData['rewards'] = rewards;
        var outSerialdata = JSON.stringify(outData);

        setTimeout(function () {
            socket.emit('serverDataToClient', outSerialdata);
        }, 300);
    });

});

function checkCollisionAndUpdateConsequences(playerDataInServer, hand, rewards)
{
    if (hand['attack'] === 1)
    {
        var attackxy = UpdateUtils.getPointInAngle(playerDataInServer.movement.targetx, playerDataInServer.movement.targety, playerDataInServer.movement.angle, 10);
        for (var key in stationaryEntities)
        {
            var sEntity = stationaryEntities[key];
            if (UpdateUtils.circularCollision(attackxy[0], attackxy[1], difficultyCircleRadius, sEntity.movement.mapx, sEntity.movement.mapy, sEntity.stats.size))
            {
                var isReduced = UpdateUtils.reduceStat(sEntity, 'health', 0, 1);
                if (isReduced)
                {
                    playerDataInServer.inventory[entityVsInventoryMap[sEntity.stats.type]]++;
                    rewards[entityVsInventoryMap[sEntity.stats.type]]++;
                    playerDataInServer.score++;
                    rewards.resourceEmpty = false;
                } else
                {
                    rewards.resourceEmpty = true;
                }

            }
        }
        for (var key in playerMap)
        {
            var player = playerMap[key];
            if (key == playerDataInServer.uniqueId) //cast and check
            {
                continue;
            }
            if (UpdateUtils.circularCollision(attackxy[0], attackxy[1], difficultyCircleRadius, player.movement.targetx, player.movement.targety, player.stats.size))
            {
                if (hand.action == 'heal')
                {
                    UpdateUtils.increaseStat(player, 'health', player.stats.maxhealth, 10);
                    player.stats.status = 'heal';
                } else if (hand.action == 'hurt')
                {
                    UpdateUtils.reduceStat(player, 'health', 0, 10);
                    player.stats.status = 'hurt';
                }
                playerDataInServer.score++;
            }
        }

        for (var key in mobileEntities)
        {
            var entity = mobileEntities[key];
            if (UpdateUtils.circularCollision(attackxy[0], attackxy[1], difficultyCircleRadius, entity.movement.mapx, entity.movement.mapy, entity.stats.size))
            {

                UpdateUtils.reduceStat(entity, 'health', 0, 10);
                if(entity.stats.health<=0)
                {
                    delete mobileEntities[key];
                }
                entity.stats.status = 'hurt';
                playerDataInServer.score++;
            }
        }
    }
}

function removeUnwantedPlayers(activeSockets) {

    for (var socketId in playerSocketMap)
    {
        if (activeSockets.indexOf(socketId) < 0)
        {
            delete playerMap[playerSocketMap[socketid]];
            delete playerSocketMap[socketid];
        }
    }

}
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use('/', mainRoute);
app.set('env', 'development');

//app.set('env', 'production');
// catch 404 and forward to error handler

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.send(err.message + '<br>' + err.stack);
    //res.render('error');
});



function onListening() {
    var addr = server.address();
    console.log("Running on port 127.0.0.1:7000");
    var bind = typeof addr === 'string'
            ? 'pipe ' + addr
            : 'port ' + addr.port;
    debug('Listening on ' + bind);
}
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string'
            ? 'Pipe ' + port
            : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}
