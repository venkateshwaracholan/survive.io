var express = require('express');
var path = require('path');
var router = express.Router();
var ROOT_DIR = path.dirname(require.main.filename);
var DOMAIN_NAME= 'http://localhost:3000';


router.get('/', function(req, res, next) {

  res.sendFile(path.join(ROOT_DIR+'/public/html/viewpoint.html'));

});

router.get('/flush', function(req, res, next) {

  res.clearCookie("leftKey");
  res.clearCookie("rightKey");
  res.send('flushed');
});

router.get('/cookie', function(req, res, next) {
  res.send(req.cookies);
});

router.get('/login', function(req, res, next) {

  //res.send(req.url);
  //app.use(express.static(path.join(__dirname+'/public/HTML/login.html')));
  //res.render(path.join(__dirname+'/public/HTML/login.html'));
  //res.setHeader("Content-Type", "text/html");
  res.cookie('leftKey', '111', { httpOnly: true });
  res.cookie('rightKey', '222');
  res.sendFile(path.join(ROOT_DIR+'/public/html/login.html'));
  //res.render('login.html');

});

router.get('/public/*/:file', function(req, res, next) {

  //res.send(req.params[0]);
  //res.sendFile(path.join(ROOT_DIR+'/' +req.params[0]));
  res.sendFile(path.join(ROOT_DIR+ '/public/' + req.params[0] + '/' + req.params.file));


});

router.get('/AAA', function(req, res, next) {

  //res.send();
  res.setHeader("Content-Type", "text/html");
  res.sendFile(path.join(ROOT_DIR+'/public/HTML/login.html'));

});

module.exports = router;
