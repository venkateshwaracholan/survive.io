/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//CAMERA





var KeyboardMouse = new function ()
{

    this.Mouse = function () {
        this.LEFTBUTTON = 0;
        this.MIDDLEBUTTON = 1;
        this.RIGHTBUTTON = 2;

        this.RELEASED = 0;
        this.PRESSED = 1;
        this.keys = {};

    }

    this.trigger_mousemove = function (e, mouse, entity, camera, view_width, view_height) {
        mouse.x = (e.clientX * getScaleRatio() * 1 / getScaleFactor()) //+ (camera.mapx - view_width/2) ;
        mouse.y = (e.clientY * getScaleRatio() * 1 / getScaleFactor()) //+(camera.mapy - view_height/2);
    }

    this.trigger_mousedown = function (e, mouse) {
        if (e.target.id != "viewpoint") {
            return;
        }
        if (e.button == mouse.LEFTBUTTON)
        {
            mouse.keys[mouse.LEFTBUTTON] = mouse.PRESSED;
        }
        if (e.button == mouse.MIDDLEBUTTON)
        {
            mouse.keys[mouse.MIDDLEBUTTON] = mouse.PRESSED;
        }
        if (e.button == mouse.RIGHTBUTTON)
        {
            mouse.keys[mouse.RIGHTBUTTON] = mouse.PRESSED;
        }
    }

    this.trigger_mouseup = function (e, mouse) {
        if (e.button == mouse.LEFTBUTTON)
        {
            mouse.keys[mouse.LEFTBUTTON] = mouse.RELEASED;
        }
        if (e.button == mouse.MIDDLEBUTTON)
        {
            mouse.keys[mouse.MIDDLEBUTTON] = mouse.RELEASED;
        }
        if (e.button == mouse.RIGHTBUTTON)
        {
            mouse.keys[mouse.RIGHTBUTTON] = mouse.RELEASED;
        }
    }

    this.Keyboard = function () {
        this.set_azerty = function () {
            this.LEFT = 81;
            this.RIGHT = 68;
            this.UP = 90;
            this.DOWN = 83;
        };

        this.set_qwerty = function () {
            this.LEFT = 65;
            this.RIGHT = 68;
            this.UP = 87;
            this.DOWN = 83;
        };

        this.RELEASED = 0;
        this.PRESSED = 1;
        this._1 = 49;
        this._2 = 50;
        this._3 = 51;
        this._4 = 52;
        this._5 = 53;
        this.CTRL = 17;
        this.ARROW_LEFT = 37;
        this.ARROW_RIGHT = 39;
        this.ARROW_UP = 38;
        this.ARROW_DOWN = 40;
        this.SPACE = 32;
        this.R = 82;
        this.G = 71;
        this.V = 86;
        this.B = 66;
        this.set_qwerty();
        this.keys = {};

        this.clear_topDowndirection = function () {
            this.keys[this.UP] = this.RELEASED;
            this.keys[this.ARROW_UP] = this.RELEASED;
            this.keys[this.DOWN] = this.RELEASED;
            this.keys[this.ARROW_DOWN] = this.RELEASED;
        };

        this.clear_leftrightdirection = function () {
            this.keys[this.RIGHT] = this.RELEASED;
            this.keys[this.ARROW_RIGHT] = this.RELEASED;
            this.keys[this.LEFT] = this.RELEASED;
            this.keys[this.ARROW_LEFT] = this.RELEASED;
        };

        this.clear_directionnal = function () {
            this.keys[this.RIGHT] = this.RELEASED;
            this.keys[this.ARROW_RIGHT] = this.RELEASED;
            this.keys[this.LEFT] = this.RELEASED;
            this.keys[this.ARROW_LEFT] = this.RELEASED;
            this.keys[this.UP] = this.RELEASED;
            this.keys[this.ARROW_UP] = this.RELEASED;
            this.keys[this.DOWN] = this.RELEASED;
            this.keys[this.ARROW_DOWN] = this.RELEASED;
        };

        this.is_left = function () {
            return this.keys[this.LEFT] || this.keys[this.ARROW_LEFT];
        };

        this.is_right = function () {
            return this.keys[this.RIGHT] || this.keys[this.ARROW_RIGHT];
        };

        this.is_top = function () {
            return this.keys[this.UP] || this.keys[this.ARROW_UP];
        };

        this.is_bottom = function () {
            return this.keys[this.DOWN] || this.keys[this.ARROW_DOWN];
        };

        this.is_ctrl = function () {
            return this.keys[this.CTRL];
        };

        this.is_1 = function () {
            return this.keys[this._1];
        }
        ;
        this.is_2 = function () {
            return this.keys[this._2];
        };

        this.is_3 = function () {
            return this.keys[this._3];
        };

        this.is_4 = function () {
            return this.keys[this._4];
        };

        this.is_space = function () {
            return this.keys[this.SPACE];
        };

        this.is_r = function () {
            return this.keys[this.R];
        };

        this.is_g = function () {
            return this.keys[this.G];
        };

        this.is_v = function () {
            return this.keys[this.V];
        };

        this.is_b = function () {
            return this.keys[this.B];
        };
    }


    this.trigger_keyup = function (e) {
        var c = Math.min(e.charCode || e.keyCode, 255);
        keyboard.keys[c] = keyboard.RELEASED;
    }

    this.trigger_keydown = function (e) {
        var c = Math.min(e.charCode || e.keyCode, 255);
        if (c == keyboard.LEFT || c == keyboard.ARROW_LEFT || c == keyboard.RIGHT || c == keyboard.ARROW_RIGHT)
        {
            keyboard.clear_leftrightdirection();
        } else if (c == keyboard.UP || c == keyboard.ARROW_UP || c == keyboard.DOWN || c == keyboard.ARROW_DOWN)
        {
            keyboard.clear_topDowndirection();
        }
        keyboard.keys[c] = keyboard.PRESSED;
    }

    this.Camera = function () {
        this.mapx = 0, this.mapy = 0;
        this.PLAYERALWAYSCENTERED = 1;
        this.PLAYERMOVETOCORNER = 2;
        this.camType = 2;

    }

    this.getXYForDrawPlayer = function (camera, entity, view_width, view_height, map_width, map_height)
    {
        if (camera.camType === camera.PLAYERALWAYSCENTERED)
        {
            return[view_width / 2, view_height / 2];
        } else if (camera.camType === camera.PLAYERMOVETOCORNER)
        {
            var xy = [entity.movement.mapx - (camera.mapx - view_width / 2), entity.movement.mapy - (camera.mapy - view_height / 2)];
            if (camera.mapx > view_width / 2 && camera.mapx < map_width - view_width / 2)
            {
                xy[0] = view_width / 2;
            }
            if (camera.mapy > view_height / 2 && camera.mapy < map_height - view_height / 2)
            {
                xy[1] = view_height / 2;
            }
            return xy;
        }

    }

    this.getXYForDrawOthers = function (camera, entity, view_width, view_height)
    {
        return[camera.mapx - view_width / 2, camera.mapy - view_height / 2];
    }


}