/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var Crafts = new function()
{
    this.list = {};
    this.list.axe = {requiredResources:{wood:15},hitPoint:2};
    var crafts = document.getElementById("crafts");
    this.showPossibleCrafts = function(hero)
    {
        //var possibleCrafts = Crafts.getPossibleWeapons(hero.inventory);
//        for(var i =0 ;i<possibleCrafts.length;i++)
//        {
//            crafts.children[i].style.display = "block";
//            crafts.children[i].className+=" axe";            
//        }
    }
    this.processCrafts = function(event)
    {
        Crafts.resourceSpent = {};
        Crafts.isCrafting = true;
        Crafts.resourceSpent.wood = 15;
        
    }
    this.getPossibleWeapons = function(inventory)
    {
        var availableResources = new Object();
        availableResources.stone = inventory.stone ? inventory.stone : 0;
        availableResources.wood = inventory.wood ? inventory.wood : 0;
        var possibleWeaponList = [];
        for(var weapon in Crafts.list)
        {
            var curWeaponResources = Crafts.list[weapon].requiredResources;
            var possible = true;
            for(var res in curWeaponResources)
            {
                if(availableResources[res] >= curWeaponResources[res])
                {
                    
                }
                else
                {
                    possible = false;
                    break;
                }
            }
            if(possible)
            {
                possibleWeaponList.push(weapon);
            }
        }
      return possibleWeaponList;
    }
}
    