
window.requestAnimFrame = (function () {
    return  window.requestAnimationFrame ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame ||
            function (callback) {
                window.setTimeout(callback, 1000 / 60);
            };
})();

//IMAGES CACHE
var stoneImg = document.getElementById("stone");
var foxImg = document.getElementById("fox");
var eyesImg = document.getElementById("eyes");
var treeImg = document.getElementById("tree");

//GLOBAL VARIABLES
var canvas = document.getElementById('viewpoint');
var scoreBoard = document.getElementById('scoreBoard').children;

var messageArea;
var map_width = 5000;
var map_height = 5000;
var objectscale = .1;
var view_scale = 3000 * objectscale;
var grid_size = 10;
var view_width = window.innerWidth;
var view_height = window.innerHeight;
var orsize = 10; // outer radius size
var hrsmultiplier = 6; // half rotation speed multiplier
canvas.width = view_width;
canvas.height = view_height;
var max_inner_width = screen.width - 65;
var max_inner_height = screen.height - 65;
canvas.style.backgroundColor = '#1E8449';
var boundary_color = '#9C640C';
var keyboard, mouse;
var ctx = canvas.getContext("2d");
var frame = 0;

var camera = new KeyboardMouse.Camera();
var hero;
var camera = new KeyboardMouse.Camera();
var socket;
var fpsmeter = new FPSMeter({decimals: 0, graph: true, theme: 'dark', position: 'absolute', top: '0%', right: '50%', left: ''});

//ENTITY
var hero;
var stationaryEntities = {};
var mobileEntities = {};
var otherPlayersMap = {};



//EVENT LISTENERS
window.onresize = resizeWindow;
function add_event_listener()
{
    window.addEventListener("mousedown", function (e) {
        KeyboardMouse.trigger_mousedown(e, mouse);
    }, !1);
    window.addEventListener("mouseup", function (e) {
        KeyboardMouse.trigger_mouseup(e, mouse);
    }, !1);
    window.addEventListener("mousemove", function (e) {
        KeyboardMouse.trigger_mousemove(e, mouse, hero, camera, view_width, view_height);
    }, !1);
    window.addEventListener("keyup", function (e) {
        KeyboardMouse.trigger_keyup(e, keyboard);
    }, !1);
    window.addEventListener("keydown", function (e) {
        KeyboardMouse.trigger_keydown(e, keyboard);
    }, !1);
}

function remove_event_listener()
{
    window.removeEventListener("mousedown", KeyboardMouse.trigger_mousedown, !1);
    window.removeEventListener("mouseup", KeyboardMouse.trigger_mouseup, !1);
    window.removeEventListener("mousemove", KeyboardMouse.trigger_mousemove, !1);
    window.removeEventListener("keyup", KeyboardMouse.trigger_keyup, !1);
    window.removeEventListener("keydown", KeyboardMouse.trigger_keydown, !1);
}


// SOCKET COMMUNICATIONS
function kickStart(name)
{
    socket = io({transports: ['websocket'], upgrade: false, reconnection: false});
//socket.disconnect();
    socket.emit('joinGame', name);
    socket.on('joinSuccess', initialize);
    socket.on('serverDataToClient', serverDataToClient);
}






function initialize(serialData)
{
    var data = JSON.parse(serialData);
    var gameprops = data[1];
    map_width = gameprops["map_width"];
    map_height = gameprops["map_height"];
    objectscale = gameprops["objectscale"];
    view_scale = 3000 * objectscale;
    grid_size = 10;
    orsize = 2;
    hrsmultiplier = 6;
    keyboard = new KeyboardMouse.Keyboard();
    mouse = new KeyboardMouse.Mouse();
    hero = data[0];
    add_event_listener();
    //setInterval(frameloop, 16);
    frameloop();
}

function frameloop()
{

    fpsmeter.tickStart();
    resizeWindow();
    update();
    communicate(hero);
    render();
    //HUD();
    requestAnimationFrame(frameloop);
    fpsmeter.tick();
    frame++;
    if (frame > 59)
    {
        frame = 0;
    }
}

function update() {

    //animation related updates only
    UpdateUtils.playerKeyBoardActions(hero, keyboard);
    UpdateUtils.followRepelTarget(hero, hero.movement.targetx, hero.movement.targety, 'follow', '');//halfspeed

    //UpdateUtils.forwardMarch(hero,mouse);
    //UpdateUtils.followTarget(hero,mouse);

    UpdateUtils.playerMouseActions(hero, mouse);
    UpdateUtils.handMovementAnimation(hero);
    UpdateUtils.hurtHeal(hero);
    UpdateUtils.turnHerotoMouseinstant(camera, hero, view_width, view_height, map_width, map_height, mouse);

    //UpdateUtils.setTargetAngle(camera, hero, view_width, view_height, map_width, map_height, mouse);
    //UpdateUtils.turnHerotoMouseslow(hero);

    UpdateUtils.playerBoundaryLimit(hero, map_width, map_height);
    UpdateUtils.updateCamera(camera, hero, view_width, view_height, map_width, map_height)
    UpdateUtils.preventMovementAfterCircularCollisionPlayer(hero, stationaryEntities);
    //UpdateUtils.preventMovementAfterCircularCollisionPlayer(hero, otherPlayersMap);
    UpdateUtils.updateRankList(otherPlayersMap, hero, scoreBoard);
    UpdateUtils.updateOtherPlayers(otherPlayersMap, map_width, map_height);
    UpdateUtils.updateMobileEntities(mobileEntities, otherPlayersMap, camera, hero, view_width, view_height, map_width, map_height);
    UpdateUtils.updateInventory(hero);
    
    Crafts.showPossibleCrafts(hero);

}

function serverDataToClient(serialData)
{
    var data = JSON.parse(serialData);
    var heroData = data['heroFromServer'];
    var otherPlayers = data['otherPlayersData'];
    stationaryEntities = data['stationaryEntities'];
    var serverMobileEntities = data['mobileEntities'];
    var rewards = data['rewards'];

    updateHeroFromServer(hero, heroData);

    updateOtherPlayersFromServer(otherPlayersMap, otherPlayers);
    updateStationaryEntitiesFromServer();
    updateMobileEntitiesFromServer(mobileEntities, serverMobileEntities);

    if (rewards.resourceEmpty)
    {
        messageAlert.showMessage("Resource Empty");
    }
    for (var player in otherPlayersMap)
    {
        if (!otherPlayers.hasOwnProperty(player))
            delete otherPlayersMap[player]
    }


}

function render()
{
    ctx.clearRect(0, 0, view_width, view_height);

    //DrawUtils.drawGrid(ctx,hero,grid_size,view_width,view_height,map_width, map_height,camera);
    if (camera.camType === camera.PLAYERALWAYSCENTERED)
    {
        DrawUtils.cameraBoundaryGrey(ctx, camera, map_width, map_height, view_width, view_height, boundary_color);
    }
    var xy = KeyboardMouse.getXYForDrawPlayer(camera, hero, view_width, view_height, map_width, map_height);
    DrawUtils.drawPlayerRound(ctx, xy[0], xy[1], hero.stats.size, hero.stats.color, hero.movement.angle, hero.lefthand.angle, hero.righthand.angle, hero.name);

    //DrawUtils.drawMomo(ctxxy[0], xy[1],hero.movement.angle);
    DrawUtils.drawOtherPlayers(ctx, otherPlayersMap, hero, view_width, view_height, camera);
    DrawUtils.drawMobileEntities(ctx, mobileEntities, hero, view_width, view_height, camera);
    DrawUtils.drawStationaryEntities(ctx, stationaryEntities, hero, view_width, view_height, camera);
    DrawUtils.drawPlayerBars(ctx, hero, view_width, view_height);
}

//update hero details from server to hero
function updateHeroFromServer(hero, heroData)
{
    hero.stats.health = heroData.stats.health;
    hero.stats.status = heroData.stats.health;
    hero.score = heroData.score;
    hero['inventory']['stone'] = heroData['inventory']['stone']
    hero['inventory']['wood'] = heroData['inventory']['wood'];
}

//update OtherPlayers details from server to client
function updateOtherPlayersFromServer(otherPlayersMap, otherPlayers) {
    for (var key in otherPlayers)
    {
        if (otherPlayersMap[key])
        {
            otherPlayersMap[key]['movement']['targetangle'] = otherPlayers[key]['movement']['angle'];
            otherPlayersMap[key]['movement']['targetx'] = otherPlayers[key]['movement']['targetx']
            otherPlayersMap[key]['movement']['targety'] = otherPlayers[key]['movement']['targety']

            if (otherPlayers[key]['lefthand']['hit'] === 1)
            {
                otherPlayersMap[key]['lefthand']['hit'] = otherPlayers[key]['lefthand']['hit']
            }
            if (otherPlayers[key]['righthand']['hit'] === 1)
            {
                otherPlayersMap[key]['righthand']['hit'] = otherPlayers[key]['righthand']['hit']
            }
            otherPlayersMap[key]['score'] = otherPlayers[key]['score']

        } else
        {
            otherPlayersMap[key] = otherPlayers[key];
        }
    }
}

function updateStationaryEntitiesFromServer() {

}

function updateMobileEntitiesFromServer(mobileEntities, serverMobileEntities)
{
    for (var key in serverMobileEntities)
    {
        if (mobileEntities[key])
        {
            //mobileEntities[key]['movement']['angle'] = serverMobileEntities[key]['movement']['angle'];
            mobileEntities[key]['movement']['targetx'] = serverMobileEntities[key]['movement']['mapx']
            mobileEntities[key]['movement']['targety'] = serverMobileEntities[key]['movement']['mapy']
            mobileEntities[key]['stats']['last_hit_by'] = serverMobileEntities[key]['stats']['last_hit_by']
            mobileEntities[key]['stats']['health'] = serverMobileEntities[key]['stats']['health']
            if (serverMobileEntities[key]['stats']['status'] != '')
            {
                mobileEntities[key]['stats']['status'] = serverMobileEntities[key]['stats']['status'];
            }
        } else
        {
            mobileEntities[key] = serverMobileEntities[key];
        }
    }
    for(var key in mobileEntities)
    {
        if(!(serverMobileEntities[key]))
        {
            delete mobileEntities[key];
        }
    }
}


function resizeWindow(e) {
    var width = window.innerWidth, height = window.innerHeight;
    canvas.width = width * getScaleRatio();
    canvas.height = height * getScaleRatio();
    max_inner_width = screen.width - 65;
    max_inner_height = screen.height - 65;
    var widthratio = max_inner_width / window.innerWidth;
    view_width = width * getScaleRatio() * 1 / getScaleFactor();
    view_height = height * getScaleRatio() * 1 / getScaleFactor();
    ctx.scale(getScaleFactor(), getScaleFactor());
}

function getScaleFactor()
{
    return  Math.max(max_inner_width, max_inner_height) / (view_scale);
}

function getScaleRatio() {
    var widthratio = max_inner_width / window.innerWidth;
    var heightratio = max_inner_height / window.innerHeight;
    return Math.min(widthratio, heightratio);
}


function HUD()
{
    var text = '';
    for (var i in keyboard.keys)
    {
        keyboard.keys[i] === keyboard.PRESSED ? text = text + '  ' + i : '';
    }
    var text = stationaryEntities['STONE_1'] ? stationaryEntities['STONE_1']['health'] : '';
    document.getElementById('cameracenter').innerHTML = 'center=' + hero.movement.mapx + ', ' + hero.movement.mapy + '<br>' +
            ' ATTACK XY: ' + (hero.movement.mapx + 10 * Math.cos(hero.movement.angle * Math.PI / 180)) + ' ' + (hero.movement.mapy + 10 * Math.sin(hero.movement.angle * Math.PI / 180))
            + '<br> STONE 1  HEALTH  =' + text;
    document.getElementById('mousedx').innerHTML = 'Mouse=' + mouse.x + ', ' + mouse.y;
}


function communicate(entity) {

    var outData = {};
    outData["hero"] = entity;
    outData["resourceSpent"] = Crafts.resourceSpent;
    setTimeout(function () {
        socket.emit('clientUpdateToServer', JSON.stringify(outData));
    }, 300);
    if (Crafts.resourceSpent) {
        Crafts.resourceSpent = null;
    }

}

var person = prompt("Please enter your name:", "UnNamed");
kickStart(person);

