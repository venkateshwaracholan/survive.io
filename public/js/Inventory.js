 
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var Inventory = new function()
{
    this.selectedStock;
    this.inventory = document.getElementById("inventory");
    this.imageMap = { "stone" : "stone","wood":"wood"};
    this.inventoryArray = new Array();
    
    this.processClick =  function(event)
    {
        if(event.target.className.includes("stock"))
        {
            if(this.selectedStock)
            {
                this.seletedStock.className.replace("selected" ,'');
               //this.selectedStock.className ="stock";
            }
            //event.target.className+= " selected";
            this.selectedStock = event.target;
        }
        else if( event.target.parentNode.className.includes("stock"))
        {
            if(this.selectedStock)
            {
               this.seletedStock.className.replace("selected" ,'');
               //this.selectedStock.className ="stock";
            }
           // event.target.parentNode.className+= " selected";
            this.selectedStock = event.target.parentNode;
        }
        event.stopPropagation();
    }
    
    this.processMouseDown = function(event)
    {
        if(event.target.className.includes("stock"))
        {
            event.target.children[0].style.bottom = "0px";
        }
        else if(event.target.parentNode.className.includes("stock"))
        {
            event.target.children[0].style.bottom = "0px";
        }
        event.stopPropagation();
    }
    
    this.processMouseUp = function(event)
    {
        if(event.target.className.includes("stock"))
        {
            event.target.children[0].style.bottom = "";
        }
        else if(event.target.parentNode.className.includes("stock"))
        {
            event.target.children[0].style.bottom = "";
        }
        event.stopPropagation();
    }
    this.addElementToArray = function(name,quantity)
    {
        var inventoryPos;
        if(this.inventoryArray.indexOf(name) == -1)
        {
            this.inventoryArray.push(name);
            inventoryPos = this.inventoryArray.length-1;
        }
        else
        {
            inventoryPos = this.inventoryArray.indexOf(name);
        }
        if(quantity <= 0)
        {
            this.inventoryArray.splice(inventoryPos,1);
            this.inventory.children[inventoryPos].className.replace(this.imageMap[name] ,'');
            this.inventory.children[inventoryPos].children[0].innerHTML = "";
        }
        else
        {
        this.inventory.children[inventoryPos].className  = "stock "+this.imageMap[name];
        this.inventory.children[inventoryPos].children[0].innerHTML  = quantity;
    }
    }
}
