/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var GameEntity = new function ()
{

    this.PlayerEntity = function(uniqueId, mapx, mapy, size, color, speed) {

        this.movement = {mapx: mapx, mapy: mapy, targetx: mapx, targety: mapy, targetangle: 0, angle: 0, curspeed : 0,maxspeed: speed,  curturnspeed:0,maxturnspeed: 10}; //target is always map target position
        this.uniqueId = uniqueId;
        this.stats = {

            size: size,
            color: color,
            health: 100,
            maxhealth: 100,
            armor: 100,
            type:'PLAYER',
            status:'',
            opacity:0,
            time:0,
            last_hit_by: ''
        };
        this.lefthand = {
            hit: 0, //hit is for animations
            action: 'hurt',
            attack: 0, //attack is for sending server the attack signal for checking
            incrementer: 4,
            maxtimeout: 30,
            time: 0,
            angle: 0
        }
        this.righthand = {
            hit: 0, //hit is for animations
            action: 'heal',
            attack: 0, //attack is for sending server the attack signal for checking
            incrementer: 4,
            maxtimeout: 30,
            time: 0,
            angle: 0
        }
        this.inventory = {
            stone: 100,
            wood:100
        }

    }

    this.StationaryEntity = function(uniqueId,x, y, size, type)
    {
        this.uniqueId = uniqueId;
        this.stats = {
            type: type,
            health: 100,
            maxhealth: size * 4,
            size: size,
            last_hit_by: ''
        }
        this.movement = {mapx: x, mapy: y};


    }

    this.MobileEntity = function(uniqueId, x, y, size, type, speed) {

        this.uniqueId = uniqueId;
        this.stats = {
            type: type,
            health: 100,
            maxhealth: 100,
            size: size,
            last_hit_by: '',
            status:'',
            opacity:0,
            time:0
        }
        this.movement = {mapx: x, mapy: y, targetx: x, targety: y, targetangle: 0, angle: 0, curspeed : 0,maxspeed: speed, curturnspeed:0,maxturnspeed: 5};
        this.lefthand = {
            hit: 0, //hit is for animations
            action: 'hurt',
            attack: 0, // while entity is attacking
            incrementer: 4,
            maxtimeout: 90,
            time: 0,
            angle: 0
        }
    }
}

module.exports = GameEntity;