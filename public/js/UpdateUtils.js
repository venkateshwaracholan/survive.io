/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var UpdateUtils = new function ()
{

    this.playerKeyBoardActions = function (entity, keyboard)
    {
        var speed = entity.movement.maxspeed;
        if ((keyboard.is_left() && keyboard.is_top()) || (keyboard.is_left() && keyboard.is_bottom())
                || (keyboard.is_right() && keyboard.is_top()) || (keyboard.is_right() && keyboard.is_bottom()))
        {
            speed = speed * .707; //dividing by 1/ √2 for normalising the diagonal movement speeds
        }
        if (keyboard.is_left())
        {
            entity.movement.targetx -= speed
        }
        if (keyboard.is_right())
        {
            entity.movement.targetx += speed;
        }
        if (keyboard.is_top())
        {
            entity.movement.targety -= speed;
        }
        if (keyboard.is_bottom())
        {
            entity.movement.targety += speed;
        }

    }

    this.playerMouseActions = function (entity, mouse)
    {
        if (entity.lefthand.hit == 0 && mouse.keys[mouse.LEFTBUTTON] == mouse.PRESSED)
        {
            entity.lefthand.hit = 1;
            //UpdateUtils.reduceStat(entity,'health',0,10);
        }
        if (entity.righthand.hit == 0 && mouse.keys[mouse.RIGHTBUTTON] == mouse.PRESSED)
        {
            entity.righthand.hit = 1;
            //UpdateUtils.increaseStat(entity,'health',100,10);
        }
    }

    this.handMovementAnimation = function (entity) {

        UpdateUtils.handAttack(entity.lefthand);
        UpdateUtils.handAttack(entity.righthand);
    }

    var x = 0;

    this.handAttack = function (hand)
    {
        hand.attack = 0;
        if (hand.hit === 1)
        {
            var incrementer = hand.incrementer;
            hand.time++;
            if (hand.time === hand.maxtimeout / 2)
            {
                hand.attack = 1;
            }
            if (hand.time > hand.maxtimeout / 2)
            {
                incrementer = -1 * incrementer;

            }
            hand.angle += incrementer;

            if (hand.time > hand.maxtimeout)
            {
                hand.time = 0;
                hand.hit = 0;
                hand.angle = 0;
            }
        }
    }
    
    this.hurtHeal = function(entity)
    {
        if(entity.stats.status==='hurt'||entity.stats.status==='heal')
        {
            var incrementer = 5;
            entity.stats.time++;
            if(entity.stats.time>20)
            {
                incrementer = -1*incrementer;
            }
            entity.stats.opacity+=incrementer;
            if (entity.stats.time >= 40)
            {
                entity.stats.time = 0;
                entity.stats.status = '';
                entity.stats.opacity=0;
            }
        }
    }


    this.setTargetAngle = function (camera, hero, view_width, view_height, map_width, map_height, mouse)
    {
        var xy = KeyboardMouse.getXYForDrawPlayer(camera, hero, view_width, view_height, map_width, map_height);
        var actdist = Math.sqrt((mouse.x - xy[0]) * (mouse.x - xy[0]) + (mouse.y - xy[1]) * (mouse.y - xy[1]));
        var angle = UpdateUtils.calculateAngle(xy[0], xy[1], mouse.x, mouse.y);
        hero.movement.targetangle = angle;
    }

    this.turnHerotoMouseinstant = function (camera, hero, view_width, view_height, map_width, map_height, mouse) {

        var xy = KeyboardMouse.getXYForDrawPlayer(camera, hero, view_width, view_height, map_width, map_height);
        var actdist = Math.sqrt((mouse.x - xy[0]) * (mouse.x - xy[0]) + (mouse.y - xy[1]) * (mouse.y - xy[1]));

        var angle = UpdateUtils.calculateAngle(xy[0], xy[1], mouse.x, mouse.y);
        hero.movement.angle = angle;

    }

    this.followRepelPoint = function (entityx, entityy, size, speed, targetx, targety, action, type)
    {
        var angle = UpdateUtils.calculateAngle(entityx, entityy, targetx, targety)
        var speed;
        if (action === 'follow')
        {
            speed = 1 * speed;
        } else if (action === 'run')
        {
            speed = -1 * speed;
        }
        entityx += speed * Math.cos(angle * Math.PI / 180);
        entityy += speed * Math.sin(angle * Math.PI / 180);

        return [entityx, entityy];
    }

    this.approach = function (max, cur, delta, action)
    {
        if (action === 'increase')
        {
            if (cur < max)
                return cur + delta;
            else
                return max;
        }
        if (action === 'decrease')
        {
            if (cur > 0)
                return cur - delta;
            else
                return 0;
        }

    }

    this.followRepelTarget = function (entity, targetx, targety, action, type) {
        var entityx = entity.movement.mapx;
        var entityy = entity.movement.mapy;
        var actdist = Math.sqrt((targetx - entityx) * (targetx - entityx) + (targety - entityy) * (targety - entityy));
        var delta = entity.movement.maxspeed / 5;
        var disttravelled = (entity.movement.curspeed / delta) * (1 + entity.movement.curspeed / delta) / 2;
        if (disttravelled < actdist)
        {
            entity.movement.curspeed = UpdateUtils.approach(entity.movement.maxspeed, entity.movement.curspeed, delta, 'increase');
        } else
        {
            entity.movement.curspeed = UpdateUtils.approach(entity.movement.maxspeed, entity.movement.curspeed, delta, 'decrease');
        }
        if (actdist < 1 * delta)
        {
            entity.movement.curspeed = actdist;
        }
        //entity.movement.curspeed = entity.movement.maxspeed;
        var xy = UpdateUtils.followRepelPoint(entityx, entityy, entity.stats.size, entity.movement.curspeed, targetx, targety, action, type);
        entity.movement.mapx = xy[0];
        entity.movement.mapy = xy[1];

    }

    this.turnHerotoMouseslow = function (entity) {

        var delta = entity.movement.maxturnspeed / 5;
        var anglediff = Math.abs((entity.movement.targetangle - entity.movement.angle + 180) % 360 - 180);
        var anglestravelled = (entity.movement.curturnspeed / delta) * (1 + entity.movement.curturnspeed / delta) / 2;
        if (anglestravelled < anglediff)
        {
            entity.movement.curturnspeed = UpdateUtils.approach(entity.movement.maxturnspeed, entity.movement.curturnspeed, delta, 'increase');
        } else
        {
            entity.movement.curturnspeed = UpdateUtils.approach(entity.movement.maxturnspeed, entity.movement.curturnspeed, delta, 'decrease');
        }
        if (anglediff < 2 * delta)
        {
            entity.movement.curturnspeed = anglediff;
        }
        //entity.movement.curturnspeed = entity.movement.maxturnspeed;
        entity.movement.angle = UpdateUtils.getnextAngleForSlowTurn(entity.movement.angle, entity.movement.targetangle, entity.movement.curturnspeed);

    }
    this.getnextAngleForSlowTurn = function (angle, targetangle, curturnspeed)
    {
        var rotationspeed = curturnspeed;

        if (angle < targetangle) {
            if (Math.abs(angle - targetangle) < 180)
            {
                angle += rotationspeed;
            } else
            {
                angle -= rotationspeed;
            }
        } else
        {
            if (Math.abs(angle - targetangle) < 180)
            {
                angle -= rotationspeed;
            } else
            {
                angle += rotationspeed;
            }
        }
        if (angle > 360)
        {
            angle = 1;
        }
        if (angle < 0)
        {
            angle = 359;
        }
        return angle;
    }
    
    this.preventMovementAfterCircularCollisionPlayer = function (player, stationaryEntities)
    {
        for (var key in stationaryEntities) {
            var stationaryEntity = stationaryEntities[key];
            var pos = UpdateUtils.calculatePoint(stationaryEntity.movement.mapx, stationaryEntity.movement.mapy, stationaryEntity.stats.size, player.movement.targetx, player.movement.targety, player.stats.size);
            player.movement.targetx = pos[0];
            player.movement.targety = pos[1];
        }

    }
    
    this.preventMovementAfterCircularCollision = function (entity, stationaryEntities)
    {
        for (var key in stationaryEntities) {
            var stationaryEntity = stationaryEntities[key];
            var pos = UpdateUtils.calculatePoint(stationaryEntity.movement.mapx, stationaryEntity.movement.mapy, stationaryEntity.stats.size, entity.movement.mapx, entity.movement.mapy, entity.stats.size);
            entity.movement.mapx = pos[0];
            entity.movement.mapy = pos[1];
        }

    }
    
    //        for (var i in players)
//        {
//            var player = players[i];
//            var pos = UpdateUtils.calculatePoint(player.movement.mapx, player.movement.mapy, player.stats.size / 2, entity.movement.mapx, entity.movement.mapy, entity.stats.size );
//            entity.movement.mapx = pos[0];
//            entity.movement.mapy = pos[1];
//        }

    this.calculatePoint = function (stonex, stoney, stoneR, playerx, playery, playerR)
    {
        var actdist = UpdateUtils.actualDistance(stonex, stoney, playerx, playery);
        var angle = UpdateUtils.calculateAngle(stonex, stoney, playerx, playery);
        if (actdist < stoneR + playerR)
        {
            playerx = stonex + (stoneR + playerR) * Math.cos(angle * Math.PI / 180)
            playery = stoney + (stoneR + playerR) * Math.sin(angle * Math.PI / 180)
        }
        return [playerx, playery];
    }

    this.playerBoundaryLimit = function (entity, map_width, map_height) {
        if (entity.movement.targetx < entity.stats.size) {
            entity.movement.targetx = entity.stats.size;
        }
        if (entity.movement.targety < entity.stats.size) {
            entity.movement.targety = entity.stats.size;
        }
        if (entity.movement.targetx > map_width - entity.stats.size) {
            entity.movement.targetx = map_width - entity.stats.size
        }
        if (entity.movement.targety > map_height - entity.stats.size) {
            entity.movement.targety = map_height - entity.stats.size
        }
    }

    this.cameraBoundaryLimit = function (camera, view_width, view_height, map_width, map_height) {
        if (camera.mapx < view_width / 2) {
            camera.mapx = view_width / 2;
        }
        if (camera.mapy < view_height / 2) {
            camera.mapy = view_height / 2;
        }
        if (camera.mapx > map_width - view_width / 2) {
            camera.mapx = map_width - view_width / 2;
        }
        if (camera.mapy > map_height - view_height / 2) {
            camera.mapy = map_height - view_height / 2;
        }
    }

    this.updateCamera = function (camera, entity, view_width, view_height, map_width, map_height)
    {
        camera.mapx = entity.movement.mapx;
        camera.mapy = entity.movement.mapy;
        if (camera.camType === camera.PLAYERMOVETOCORNER)
        {
            UpdateUtils.cameraBoundaryLimit(camera, view_width, view_height, map_width, map_height);
        }

    }

    this.calculateAngle = function (x, y, targetx, targety) {

        var xDistance = targetx - x;
        var yDistance = targety - y;
        return  (360 + (180 / Math.PI) * Math.atan2(yDistance, xDistance)) % 360;

    }

    this.updateOtherPlayers = function (otherPlayersMap, map_width, map_height)
    {
        for (var i in otherPlayersMap)
        {
            var player = otherPlayersMap[i];
            UpdateUtils.otherplayerMove(player);
            UpdateUtils.handMovementAnimation(player);
            UpdateUtils.playerBoundaryLimit(player, map_width, map_height);
        }
    }

    this.updateMobileEntities = function (mobileEntities, otherPlayersMap, camera, hero, view_width, view_height, map_width, map_height)
    {
        for (var i in mobileEntities)
        {
            var entity = mobileEntities[i];
            UpdateUtils.followRepelTarget(entity, entity.movement.targetx, entity.movement.targety, 'follow', '');//halfspeed
            UpdateUtils.hurtHeal(entity);
            if (entity.stats.last_hit_by != '')
            {
                if (entity.stats.last_hit_by == hero.uniqueId)//cast and check
                {
                    var xy = KeyboardMouse.getXYForDrawPlayer(camera, hero, view_width, view_height, map_width, map_height);
                    var animalxy = KeyboardMouse.getXYForDrawOthers(camera, entity, view_width, view_height);
                    var animalscreenx = entity.movement.mapx - animalxy[0];
                    var animalscreeny = entity.movement.mapy - animalxy[1];
                    entity.movement.angle = UpdateUtils.calculateAngle(animalscreenx, animalscreeny, xy[0], xy[1]);

                } else if (otherPlayersMap[entity.stats.last_hit_by])
                {
                    var player = otherPlayersMap[entity.stats.last_hit_by];
                    var playerxy = KeyboardMouse.getXYForDrawOthers(camera, player, view_width, view_height);
                    var animalxy = KeyboardMouse.getXYForDrawOthers(camera, entity, view_width, view_height);
                    var animalscreenx = entity.movement.mapx - animalxy[0];
                    var animalscreeny = entity.movement.mapy - animalxy[1];
                    var playerscreenx = player.movement.mapx - playerxy[0];
                    var playerscreeny = player.movement.mapy - playerxy[1];
                    entity.movement.angle = UpdateUtils.calculateAngle(animalscreenx, animalscreeny, playerscreenx, playerscreeny);
                }
            }



            //UpdateUtils.turnHerotoMouseslow(entity);
        }
    }

    this.otherplayerMove = function (entity)
    {
        UpdateUtils.followRepelTarget(entity, entity.movement.targetx, entity.movement.targety, 'follow', '');
        UpdateUtils.turnHerotoMouseslow(entity);
    }

    this.actualDistance = function (x1, y1, x2, y2) {
        return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    }

    this.getstartx = function (x, view_width) {
        return x - view_width / 2;
    }
    this.getstarty = function (y, view_height) {
        return y - view_height / 2;
    }
    this.getendx = function (x, view_width) {
        return x + view_width / 2;
    }
    this.getendy = function (y, view_height) {
        return y + view_height / 2;
    }
// cx,cy is the camera or hero
    this.checkCircleInView = function (x, y, cx, cy, view_width, view_height, circlesize) {
        if (x + circlesize > (cx - view_width / 2) && x - circlesize < (cx + view_width / 2) && y + circlesize > (cy - view_height / 2) && y - circlesize < (cy + view_height / 2))
        {
            return true;
        } else {
            return false;
        }
    }

    this.updatePlayerBars = function (hero)
    {

    }

    this.reduceStat = function (entity, statName, min, amt)
    {

        entity['stats'][statName] -= amt;
        if (entity['stats'][statName] < min)
        {
            entity['stats'][statName] = min;
            return false;
        }
        return true;
    }

    this.increaseStat = function (entity, statName, max, amt)
    {

        entity['stats'][statName] += amt;
        if (entity['stats'][statName] > max)
        {
            entity['stats'][statName] = max;
            return false;
        }
        return true;
    }

    this.getPointInAngle = function (x, y, angle, range)
    {
        return [x + range * Math.cos(angle * Math.PI / 180), y + range * Math.sin(angle * Math.PI / 180)]
    }

    this.circularCollision = function (x1, y1, r1, x2, y2, r2)
    {
        var actDist = UpdateUtils.actualDistance(x1, y1, x2, y2);
        if (actDist <= r1 + r2)
        {
            return true;
        } else
        {
            return  false;
        }
    }

    this.updateRankList = function (topPlayerList, hero, scoreBoard)
    {
        var tempLength = 0;
        var scoreObjArray = [];
        for (var player in topPlayerList)
        {

            scoreObjArray.push(UpdateUtils.getScoreObject(topPlayerList[player].score, topPlayerList[player]));
//        scoreBoard[tempLength+1].children[1].innerHTML = topPlayerList[i].score;
        }
        scoreObjArray.push(UpdateUtils.getScoreObject(hero.score, hero));
        scoreObjArray.sort(function (a, b) {
            return b.score - a.score;
        })

        for (var i = 0; i < 10; i++)
        {
            if (!scoreObjArray[i])
            {
                break;
            }
            scoreBoard[i + 1].children[0].innerHTML = i + 1 + ". " + scoreObjArray[i].name;
            scoreBoard[i + 1].children[1].innerHTML = scoreObjArray[i].score;
        }

    }

    this.getScoreObject = function (id, player)
    {
        var scoreObj = new Object();
        scoreObj.uniqueId = id;
        scoreObj.score = player.score;
        scoreObj.name = player.name;
        return scoreObj;
    }

    this.updateInventory = function (hero) {
        for (var key in hero.inventory) {
            Inventory.addElementToArray(key, hero.inventory[key])
        }
    }
}


module.exports = UpdateUtils;