/* 
 * To change this license header,( choose License Headers in Project Properties.
 * To change this template file,( choose Tools | Templates
 * and open the template in the editor.
 */

DrawUtils = new function ()
{

    this.drawCircle = function (ctx, x, y, xoffset, yoffset, r, angle, color, strokewidth,strokecolor, fillstroke) {

        ctx.translate(x, y);
        ctx.rotate(angle * Math.PI / 180);
        ctx.beginPath();
        ctx.lineWidth = strokewidth;
        ctx.arc(xoffset, yoffset, r, 0, 2 * Math.PI, false);
        ctx.fillStyle = color;
        ctx.strokeStyle = strokecolor;
        if (fillstroke.toLowerCase().indexOf('fill') !== -1)
        {
            ctx.fill();
        }
        if (fillstroke.toLowerCase().indexOf('stroke') !== -1)
        {
            ctx.stroke();
        }
        ctx.rotate(-angle * Math.PI / 180);
        ctx.translate(-x, -y);
    }

    this.drawText = function (ctx, x, y, xoffset, yoffset, angle, name, fontsize, fontname, textcolor, textalign) {

        ctx.translate(x, y);
        ctx.rotate(angle * Math.PI / 180);
        ctx.fillStyle = textcolor;
        ctx.font = 600 + ' ' + fontsize + 'px ' + fontname;
        ctx.textAlign = textalign;
        ctx.fillText(name, xoffset, yoffset);
        ctx.rotate(-angle * Math.PI / 180);
        ctx.translate(-x, -y);
    }



    this.roundRect = function (ctx, x, y, xo, yo, w, h, radius, angle, color, strokewidth, strokecolor, fillstroke) {

        ctx.translate(x, y);
        ctx.rotate(angle * Math.PI / 180);
        ctx.fillStyle = color;
        ctx.strokeStyle = strokecolor;
        ctx.lineWidth = strokewidth;

        ctx.beginPath();
        ctx.moveTo(0 + radius.tl + xo, 0 + yo);
        ctx.lineTo(w - radius.tr + xo, 0 + yo);
        ctx.arcTo(w + xo, 0 + yo, w + xo, h + radius.tr + yo, radius.tr);
        ctx.lineTo(w + xo, h - radius.br + yo);
        ctx.arcTo(w + xo, h + yo, w - radius.br + xo, h + yo, radius.br);
        ctx.lineTo(0 + radius.bl + xo, h + yo);
        ctx.arcTo(0 + xo, h + yo, 0 + xo, h - radius.bl + yo, radius.bl);
        ctx.lineTo(0 + xo, 0 + radius.tl + yo);
        ctx.arcTo(0 + xo, 0 + yo, 0 + radius.tl + xo, 0 + yo, radius.tl);
        ctx.closePath();

        if (fillstroke.toLowerCase().indexOf('fill') !== -1)
        {
            ctx.fill();
        }
        if (fillstroke.toLowerCase().indexOf('stroke') !== -1)
        {
            ctx.stroke();
        }
        ctx.rotate(-angle * Math.PI / 180);
        ctx.translate(-x, -y);
    }

    this.drawPicture = function (ctx, x, y, xo, yo, w, h, angle, img)
    {
        ctx.translate(x, y);
        ctx.rotate((angle - 90) * Math.PI / 180);
        ctx.drawImage(img, xo, yo, w, h);
        ctx.rotate(-(angle - 90) * Math.PI / 180);
        ctx.translate(-x, -y);
    }

    this.drawRect = function (ctx, x, y, px, py, w, h, angle, color) {
        ctx.translate(x, y);
        ctx.rotate(angle * Math.PI / 180);
        ctx.fillStyle = color;
        ctx.fillRect(px, py, w, h);
        ctx.rotate(-angle * Math.PI / 180);
        ctx.translate(-x, -y);
    }

    this.drawMomo = function (ctx, x, y, angle) {

        ctx.translate(x, y);
        ctx.rotate((angle - 90) * Math.PI / 180);
        ctx.beginPath();
        var scale = .1;
        var xoff = -218;
        var yoff = -189;

        ctx.moveTo((178 + xoff) * scale, (176 + yoff) * scale);
        ctx.bezierCurveTo((152 + xoff) * scale, (196 + yoff) * scale, (121 + xoff) * scale, (98 + yoff) * scale, (145 + xoff) * scale, (85 + yoff) * scale);
        ctx.bezierCurveTo((133 + xoff) * scale, (111 + yoff) * scale, (203 + xoff) * scale, (137 + yoff) * scale, (190 + xoff) * scale, (159 + yoff) * scale);
        ctx.bezierCurveTo((192 + xoff) * scale, (149 + yoff) * scale, (231 + xoff) * scale, (139 + yoff) * scale, (249 + xoff) * scale, (157 + yoff) * scale);
        ctx.bezierCurveTo((239 + xoff) * scale, (119 + yoff) * scale, (294 + xoff) * scale, (122 + yoff) * scale, (298 + xoff) * scale, (90 + yoff) * scale);
        ctx.bezierCurveTo((315 + xoff) * scale, (110 + yoff) * scale, (285 + xoff) * scale, (181 + yoff) * scale, (262 + xoff) * scale, (177 + yoff) * scale);
        ctx.bezierCurveTo((263 + xoff) * scale, (229 + yoff) * scale, (234 + xoff) * scale, (243 + yoff) * scale, (221 + xoff) * scale, (241 + yoff) * scale);
        ctx.bezierCurveTo((206 + xoff) * scale, (243 + yoff) * scale, (174 + xoff) * scale, (230 + yoff) * scale, (178 + xoff) * scale, (178 + yoff) * scale);
        ctx.strokeStyle = "black";
        ctx.stroke();
        ctx.rotate((-angle + 90) * Math.PI / 180);
        ctx.translate(-x, -y);
    }

    this.drawGrid = function (ctx, hero, grid_size, view_width, view_height, map_width, map_height, camera) {
        var xy = KeyboardMouse.getXYForDrawOthers(camera, hero, view_width, view_height);
        ctx.translate(-xy[0] % grid_size, -xy[1] % grid_size);
        ctx.beginPath();
        ctx.lineWidth = .04;
        for (var x = 0; x <= view_width + grid_size; x += grid_size) {
            ctx.moveTo(x, 0);
            ctx.lineTo(x, view_height + grid_size);
        }

        for (var x = 0; x <= view_height + grid_size; x += grid_size) {
            ctx.moveTo(0, x);
            ctx.lineTo(view_width + grid_size, x);
        }

        ctx.strokeStyle = "grey";
        ctx.stroke();
        ctx.translate(+xy[0] % grid_size, +xy[1] % grid_size);
    }

    this.drawPlayer = function (ctx, x, y, size, color, angle, langle, rangle, name) {

        var strokecolor = 'darkgreen';
        DrawUtils.roundRect(ctx, x, y, -size , -size , size*2, size*2, {tl: 2, tr: 2, bl: 2, br: 2}, angle, color, .7, strokecolor, 'fill_stroke');

        var img = document.getElementById("mask");
        var img2 = document.getElementById("teeth");
        DrawUtils.drawPicture(ctx, x, y, -15 / 2, -20 / 2, 15, 15, angle, img);
        DrawUtils.drawPicture(ctx, x, y, -10 / 2, -6 / 2, 10, 10, angle, img2);

        DrawUtils.roundRect(ctx, x, y, -size * .7 / 2, -size * .8 / 2 + 10, size * .7, size * .8, {tl: 1.5, tr: 1.5, bl: 1.5, br: 1.5}, angle - langle, color, .7, strokecolor, 'fill_stroke');
        DrawUtils.roundRect(ctx, x, y, -size * .7 / 2, -size * .8 / 2 - 10, size * .7, size * .8, {tl: 1.5, tr: 1.5, bl: 1.5, br: 1.5}, angle + rangle, color, .7, strokecolor, 'fill_stroke');
        var xy = UpdateUtils.getPointInAngle(x, y, angle, 10);
        DrawUtils.drawCircle(ctx, xy[0], xy[1], 0, 0, 2, 0, color, 1, color,'fill');
        DrawUtils.drawText(ctx, x, y - 10, 0, 0, 0, name, 4, 'Arial black', '#FBFCFC', 'center');

    }
    
    this.drawPlayerRound = function (ctx, x, y, size, color, angle, langle, rangle, name) {

        var strokecolor = 'darkgreen';
        //DrawUtils.roundRect(ctx, x, y, -size , -size , size*2, size*2, {tl: 2, tr: 2, bl: 2, br: 2}, angle, color, .7, strokecolor, 'fill_stroke');
        DrawUtils.drawCircle(ctx, x, y,0, 0, size, angle, color, .7, strokecolor, 'fill');
        DrawUtils.drawCircle(ctx, x, y,0, 0, size-.2, angle, color, .7, strokecolor, 'stroke');
        DrawUtils.drawPicture(ctx, x, y, -8 / 2, 0, 8, 3.7, angle, eyesImg);

        //DrawUtils.roundRect(ctx, x, y, -size * .7 / 2, -size * .8 / 2 + 10, size * .7, size * .8, {tl: 1.5, tr: 1.5, bl: 1.5, br: 1.5}, angle - langle, color, .7, strokecolor, 'fill_stroke');
        //DrawUtils.roundRect(ctx, x, y, -size * .7 / 2, -size * .8 / 2 - 10, size * .7, size * .8, {tl: 1.5, tr: 1.5, bl: 1.5, br: 1.5}, angle + rangle, color, .7, strokecolor, 'fill_stroke');
        DrawUtils.drawCircle(ctx, x, y, 2, +9,size * .35, angle - langle, color, .7, strokecolor, 'fill');
        DrawUtils.drawCircle(ctx, x, y, 2, - 9, size * .35, angle + rangle, color, .7, strokecolor, 'fill');
        DrawUtils.drawCircle(ctx, x, y, 2, +9,size * .35 - .2, angle - langle, color, .6, strokecolor, 'stroke');
        DrawUtils.drawCircle(ctx, x, y, 2, - 9, size * .35 - .2, angle + rangle, color, .6, strokecolor, 'stroke');
        var xy = UpdateUtils.getPointInAngle(x, y, angle, 10);
        DrawUtils.drawCircle(ctx, xy[0], xy[1], 0, 0, 2, 0, color,1, color,'fill');
        var xy = UpdateUtils.getPointInAngle(x, y, angle, 15);
        DrawUtils.drawCircle(ctx, xy[0], xy[1], 0, 0, 2, 0, color,1, color,'fill');
        var xy = UpdateUtils.getPointInAngle(x, y, angle, 20);
        DrawUtils.drawCircle(ctx, xy[0], xy[1], 0, 0, 2, 0, color,1, color,'fill');
        DrawUtils.drawText(ctx, x, y - 10, 0, 0, 0, name, 4, 'Arial black', '#FBFCFC', 'center');

    }

    this.drawPlayerBars = function (ctx, hero, view_width, view_height)
    {
        color = '#2C3E50';

        var w = 60, h = 30;
        ctx.globalAlpha = 0.8;
        DrawUtils.roundRect(ctx, 0, view_height - 26 , 0, 0, 70, 26, {tl: 0, tr: 1.2, bl: 1.2, br: 0}, 0, color, 0.0001, color, 'fill_stroke');
        ctx.globalAlpha = 1;
        var bordercolor = '#F4F6F7';
        
        //food
        DrawUtils.drawText(ctx, 24, view_height - 22, 0, 0, 0, "FOOD", 3, 'Arial', '#ECF0F1', 'center');
        DrawUtils.drawText(ctx, 24, view_height - 18.5, 0, 0, 0, "100%", 3, 'Arial', '#ECF0F1', 'center');
        
        //water
        DrawUtils.drawText(ctx, 42, view_height - 22, 0, 0, 0, "WATER", 3, 'Arial', '#ECF0F1', 'center');
        DrawUtils.drawText(ctx, 42, view_height - 18.5, 0, 0, 0, "100%", 3, 'Arial', '#ECF0F1', 'center');
        
        //temperature
        DrawUtils.drawText(ctx, 60, view_height - 22, 0, 0, 0, "TEMP", 3, 'Arial', '#5DADE2', 'center');
        DrawUtils.drawText(ctx, 60, view_height - 18.5, 0, 0, 0, "30°C", 3, 'Arial', '#5DADE2', 'center');
        
        //health
        DrawUtils.drawText(ctx, 1.3, view_height - 12.6, 0, 0, 0, "HEALTH", 3, 'Arial', '#FBFCFC', 'start');
        var h = hero.stats.health / hero.stats.maxhealth;
        if (h != 0)
        {
            DrawUtils.roundRect(ctx, 17, view_height - 15, 0, 0, 50 * h, 2.7, {tl: 1.5, tr: 1.5, bl: 1.5, br: 1.5}, 0, '#E74C3C', 0.0001, bordercolor, 'fill');
        }
        DrawUtils.roundRect(ctx, 17, view_height - 15, 0, 0, 50, 2.7, {tl: 1.5, tr: 1.5, bl: 1.5, br: 1.5}, 0, bordercolor, 0.3, bordercolor, 'stroke');
        
        //mana
        DrawUtils.drawText(ctx, 1.3, view_height - 7.6, 0, 0, 0, "MANA", 3, 'Arial', '#FBFCFC', 'start');
        DrawUtils.roundRect(ctx, 17, view_height - 10, 0, 0, 50, 2.7, {tl: 1.5, tr: 1.5, bl: 1.5, br: 1.5}, 0, '#3498DB', 0.0001, bordercolor, 'fill');
        DrawUtils.roundRect(ctx, 17, view_height - 10, 0, 0, 50, 2.7, {tl: 1.5, tr: 1.5, bl: 1.5, br: 1.5}, 0, bordercolor, 0.3, bordercolor, 'stroke');
        
        //stamina
        DrawUtils.drawText(ctx, 1.3, view_height - 2.6, 0, 0, 0, "STAMINA", 3, 'Arial', '#FBFCFC', 'start');
        DrawUtils.roundRect(ctx, 17, view_height - 5, 0, 0, 50, 2.7, {tl: 1.5, tr: 1.5, bl: 1.5, br: 1.5}, 0, '#27AE60', 0.0001, bordercolor, 'fill');
        DrawUtils.roundRect(ctx, 17, view_height - 5, 0, 0, 50, 2.7, {tl: 1.5, tr: 1.5, bl: 1.5, br: 1.5}, 0, bordercolor, 0.3, bordercolor, 'stroke');
        
        
        
    }

    this.cameraBoundaryGrey = function (ctx, camera, map_width, map_height, view_width, view_height, boundary_color) {
        var strx = UpdateUtils.getstartx(camera.mapx, view_width), stry = UpdateUtils.getstarty(camera.mapy, view_height), endx = UpdateUtils.getendx(camera.mapx, view_width), endy = UpdateUtils.getendy(camera.mapy, view_height);

        if (camera.mapx < view_width / 2) {
            DrawUtils.drawRect(ctx, 0, 0, 0, 0, -strx, view_height, 0, boundary_color);
        }
        if (camera.mapy < view_height / 2) {
            DrawUtils.drawRect(ctx, 0, 0, 0, 0, view_width, -stry, 0, boundary_color);
        }
        if (endx > map_width) {
            DrawUtils.drawRect(ctx, map_width - strx, 0, 0, 0, endx - map_width, view_height, 0, boundary_color);
        }
        if (endy > map_height) {
            DrawUtils.drawRect(ctx, 0, map_height - stry, 0, 0, view_width, endy - map_height, 0, boundary_color);

        }
    }

    this.drawMobileEntities = function (ctx, mobileEntities, entity, view_width, view_height, camera) {
        var xy = KeyboardMouse.getXYForDrawOthers(camera, entity, view_width, view_height);
        for (var key in mobileEntities) {
            var mobileEntity = mobileEntities[key];
            var screenx = mobileEntity.movement.mapx - xy[0];
            var screeny = mobileEntity.movement.mapy - xy[1];
            if (UpdateUtils.checkCircleInView(mobileEntity.movement.mapx, mobileEntity.movement.mapy, camera.mapx, camera.mapy, view_width, view_height, mobileEntity.stats.size)) {
                DrawUtils.drawPicture(ctx, screenx, screeny,
                -mobileEntity.stats.size*1.5,-mobileEntity.stats.size*1.5, mobileEntity.stats.size*3, mobileEntity.stats.size*3, mobileEntity.movement.angle,foxImg)
                var color = "rgba(255, 0, 0, " + mobileEntity.stats.opacity/150+ ")";
                DrawUtils.drawCircle(ctx, mobileEntity.movement.mapx - xy[0], mobileEntity.movement.mapy - xy[1], 0, 0, mobileEntity.stats.size, 0,color,1,'red', 'fill');
                DrawUtils.drawText(ctx, screenx, screeny - 10, 0, 0, 0, mobileEntity.stats.health, 4, 'Arial black', '#FBFCFC', 'center');

            }
        }
    }
    
    this.drawStationaryEntities = function (ctx, stationaryEntities, entity, view_width, view_height, camera) {
        var xy = KeyboardMouse.getXYForDrawOthers(camera, entity, view_width, view_height);
        for (var key in stationaryEntities) {
            var stationaryEntity = stationaryEntities[key];
            
            if (UpdateUtils.checkCircleInView(stationaryEntity.movement.mapx, stationaryEntity.movement.mapy, camera.mapx, camera.mapy, view_width, view_height, stationaryEntity.stats.size)) {
                var image = stationaryEntity.stats.type === "STONE" ? stoneImg : treeImg;
                DrawUtils.drawPicture(ctx, stationaryEntity.movement.mapx - xy[0], stationaryEntity.movement.mapy - xy[1],-stationaryEntity.stats.size,-stationaryEntity.stats.size, stationaryEntity.stats.size*2, stationaryEntity.stats.size*2, 125,image)
                //DrawUtils.drawCircle(ctx, stationaryEntity.movement.mapx - xy[0], stationaryEntity.movement.mapy - xy[1], 0, 0, stationaryEntity.stats.size, 0, stationaryEntity.stats.color, 1,stationaryEntity.stats.color, 'fill');
            }
        }
    }

    this.drawOtherPlayers = function (ctx, players, entity, view_width, view_height, camera)
    {
        for (var i in players)
        {
            var player = players[i];
            var xy = KeyboardMouse.getXYForDrawOthers(camera, entity, view_width, view_height);
            if (UpdateUtils.checkCircleInView(player.movement.mapx, player.movement.mapy, camera.mapx, camera.mapy, view_width, view_height, player.stats.size)) {
                DrawUtils.drawPlayerRound(ctx, player.movement.mapx - xy[0], player.movement.mapy - xy[1], player.stats.size, player.stats.color, player.movement.angle, player.lefthand.angle, player.righthand.angle, player.name);
            }
        }
    }

}